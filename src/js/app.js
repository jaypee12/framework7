// Import Vue
import Vue from "vue";

window.moment = require("moment");

window._ = require("lodash");

// Import Axios
var key = "yz8pgGNNdwEQVHvGweMJpFA23Abbn6Fr5bzMJzBn";
var nonce = moment().unix();
var crypto = require("crypto-js");
var signature = moment().unix() + key + "uxG49mS45XJvLSxwHkDVp9mrrTfQnNsf";

window.axios = require("axios");
axios.defaults.headers.common["API_KEY"] = key;
axios.defaults.headers.common["ACCESS_NONCE"] = nonce;
axios.defaults.headers.common["Access-Control-Allow-Origin"] = "*";
axios.defaults.headers.common["API_SIGNATURE"] = crypto.HmacSHA1(
  signature,
  "uxG49mS45XJvLSxwHkDVp9mrrTfQnNsf"
);

// Import Framework7
import Framework7 from "framework7/framework7-lite.esm.bundle.js";

// Import Framework7-Vue Plugin
import Framework7Vue from "framework7-vue/framework7-vue.esm.bundle.js";

// Import Framework7 Styles
import "framework7/css/framework7.bundle.css";

// Import Icons and App Custom Styles
import "../css/icons.css";
import "../css/app.scss";
import "../css/animate.css";

// Import App Component
import App from "../components/app.vue";

// Import Alert Component
import Alert from "../components/tools/alert.vue";
Vue.component("AlertMessage", Alert);

//vuex library starlized variable storage
import store from "./store";

//validation for vue
import Vuelidate from "vuelidate";
Vue.use(Vuelidate);

// Init Framework7-Vue Plugin
Framework7.use(Framework7Vue);

// Init App
new Vue({
  el: "#app",
  render: h => h(App),
  store,
  // Register App Component
  components: {
    app: App
  }
});
