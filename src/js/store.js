import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    user: {
      username: "",
      fname: "",
      lname: ""
    }
  },
  getters: {
    getUser(state) {
      return state.user;
    }
  },
  mutations: {
    setUser(state, values) {
      state.user.username = values.username;
      state.user.fname = values.fname;
      state.user.lname = values.lname;
    }
  }
});

export default store;
