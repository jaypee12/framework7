//vuex store global variables
import store from "./store";
var routes = [
  {
    path: "/",
    redirect: "/auth/"
  },
  {
    path: "/auth/",
    asyncComponent: () => import("../pages/auth.vue"),
    beforeEnter: (outeTo, routeFrom, resolve, reject) => {
      if (_.isEmpty(store.state.user.username)) {
        resolve();
      } else {
        alert(1);
        reject("/home/");
      }
    }
  },
  {
    path: "/login/",
    asyncComponent: () => import("../pages/auth/login.vue"),
    beforeEnter: (outeTo, routeFrom, resolve, reject) => {
      if (_.isEmpty(store.state.user.username)) {
        resolve();
      } else {
        reject("/home/");
      }
    }
  },
  {
    path: "/register/",
    asyncComponent: () => import("../pages/auth/register/register.vue")
  },
  {
    path: "/home/",
    asyncComponent: () => import("../pages/home.vue"),
    beforeEnter: (outeTo, routeFrom, resolve, reject) => {
      console.log(1);
      if (_.isEmpty(store.state.user.username)) {
        reject();
      } else {
        resolve();
      }
    },
    on: {
      pageBeforeIn: function(event, page) {
        console.log(page);
      }
    },
    routes: [
      {
        path: "/profile/",
        asyncComponent: () => import("../pages/profile.vue")
      },
      {
        path: "/notifications/",
        asyncComponent: () => import("../pages/notification.vue")
      },

      {
        path: "/trade/",
        asyncComponent: () => import("../pages/trade.vue"),
        routes: [
          {
            path: "/redeem/",
            asyncComponent: () => import("../pages/notification.vue")
          },
          {
            path: "/purchase/",
            asyncComponent: () => import("../pages/trade/purchase/purchase.vue")
          }
        ]
      },

      {
        path: "/history/",
        asyncComponent: () => import("../pages/history.vue")
      }
    ]
  },

  //    {
  //        path: "/settings/",
  //        component: Settings
  //  },

  {
    path: "(.*)",
    asyncComponent: () => import("../pages/404.vue")
  }
];

export default routes;
